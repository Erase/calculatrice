/*
    Fonctionnalités liées à la calculatrice
 */
//~ Au chargement du DOM : 
document.addEventListener("DOMContentLoaded", function() {

    /*
        On stock nos sélecteurs dans des variables
        pour un usage futur : less is more
     */
    var resultat    = document.getElementById('resultat'); //~ Zone de résultat d'opération
    var saisie      = document.getElementById('saisie');    //~ Zone de saisie en cours
    var _buttons    = document.querySelectorAll('input[type=button'); //~ Tous les boutons de la calculatrice
    var is_raz_next = false; //~ Permet de savoir si la prochaine saisie provequera une RAZ
    
    //~ On lance l'initialisation 
    init();

    //~ Déclenchement de base
    function init(){
        //~ On assigne un évènement à chaque bouton : tu cliques ou tu meures
        _buttons.forEach(button => {
            button.addEventListener('click', function(){
                //~ Go go go le calcul \o/
                calcul(button);
            });
        });
    }

    /*
        Function ajoute_saisie
        Add value to capture area
        @param : (int) valeur,  button value
        @return : none
     */
    function ajoute_saisie(valeur){
        saisie.value += valeur;
    }

    /*
        Function get_resultat
        Force the operation and inject result in result area
        @param : none
        @return : none
     */
    function get_resultat(){
        resultat.value = eval(saisie.value);
    }

    /*
        Function calcul
        Perform the operation or result
        @param : (object) button element
        @return none
     */
    function calcul(element){
        /*
            Si on clic sur égale, la prochaine valeur saisie
            sera une nouvelle opération
         */
        var valeur = element.value;
        if(valeur !== "="){
            if(is_raz_next){
                saisie.value = "";
                is_raz_next = false; 
            }
            ajoute_saisie(valeur)
        }else{
            get_resultat();
            is_raz_next = true;
        }
    }

});